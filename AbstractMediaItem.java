import java.io.Serializable;
import java.util.GregorianCalendar;
/**
 * Defines an abstract media item in a library system.
 * 
 * @author Ben Hoff
 * @version 1.0
 */

public abstract class AbstractMediaItem implements MediaItem, Serializable
{
    // instance variables
    private String callNumber;
    private int copyNumber;
    private String title;
    private GregorianCalendar dueDate;
    private String borrower;
    private double lateFee;
    /**
     * Constructor for objects of class AbstractMediaItem
     */
    public AbstractMediaItem()
    {
        this.copyNumber = 0;
        this.lateFee =  0.25;
    }
    
    /**
     * Returns the validity of the borrower string
     * 
     * @return  if `inBorrower` is valid or not
     */
    private boolean validBorrower(String inBorrower)
    {
        if (inBorrower == null)
        {
            return true;
        }
        return !inBorrower.isEmpty();
    }
   
    /**
     * Returns the validity of the call number
     * 
     * @return  if `inCallNumber` is valid or not
     */
    private boolean validCallNumber(String inCallNumber)
    {
        if (inCallNumber == null)
        {
            return false;
        }
        else if (inCallNumber.isEmpty())
        {
            return false;
        }
        else if (inCallNumber.split("\\s+").length > 2)
        {
            return false;
        }
        else if (inCallNumber.split("\\.").length > 2)
        {
            return false;
        }
        
        return inCallNumber.matches("^[a-zA-Z0-9 .]+$");
    }

    /**
     * Returns the validity of the copy number
     * 
     * @return  if `inCopyNo` is valid or not
     */
    private boolean validCopyNumber(int inCopyNo)
    {
        // System.out.println(inCopyNo);
        return inCopyNo >= 0;
    }
    
    /**
     * Returns the validity of the title
     * 
     * @return  if `title` is valid or not
     */   
    private boolean validTitle(String inTitle)
    {
        if (inTitle == null)
        {
            return false;
        }
        return !inTitle.isEmpty();
    }
    
    /**
     * Returns the call number of the item.
     * 
     * @return      the call number. 
     */
    public String getCallNumber()
    {
        return this.callNumber;
    }
    
    /**
     * Sets the call number. 
     * 
     * @param   callNo the call number for the item
     */
    public void setCallNumber(String callNo)
    {
        if (!this.validCallNumber(callNo))
        {
            throw new IllegalArgumentException();
        }
        
        this.callNumber = callNo;
    }
    
    /**
     * Returns the copy number of the item.
     * 
     * @return  the copy number.
     */
    public int getCopyNumber()
    {
        return this.copyNumber;
    }

    /**
     * Sets copy number of the item.
     * 
     * @param   copy    the copy number.
     */
    public void setCopyNumber(int copy)
    {
        if (!this.validCopyNumber(copy))
        {
            throw new IllegalArgumentException();
        }
        this.copyNumber = copy;
    }

    /**
     * Returns the title of the item.
     * 
     * @return  the title of the item.
     */
    public String getTitle()
    {
        return this.title;
    }
    
    /**
     * Sets the title of the item.
     * 
     * @param   itemTitle  the title.
     */
    public void setTitle(String itemTitle)
    {
        if (!this.validTitle(itemTitle))
        {
            throw new IllegalArgumentException();
        }
        
        this.title = itemTitle;
    }

    /**
     * Returns the due date.
     * 
     * @return  the due date.
     */
    public GregorianCalendar getDueDate()
    {
        return this.dueDate;
    }
    
    /**
     * Sets the due date.
     * 
     * @param   date    the due date.
     */
    public void setDueDate(GregorianCalendar date)
    {
        this.dueDate = date;
    }
    
    /**
     * Returns the id of the person who borrowed the item.
     * 
     * @return  the borrower id. 
     */
    public String getBorrower()
    {
        return this.borrower;
    }

    /**
     * Set the borrower. 
     * 
     * @param   borrowerId    the id of the borrower.
     */
    public void setBorrower(String borrowerId)
    {
        if (!this.validBorrower(borrowerId))
        {
            throw new IllegalArgumentException();
        }
        
        this.borrower = borrowerId;
    }
   
    /**
     * Set the late fee. 
     * 
     * @param   newFee    the value of the fee.
     */
    protected void setLateFee(double newFee)
    {
        if (newFee < 0.)
        {
            throw new IllegalArgumentException();
        }
        
        this.lateFee = newFee;
    }

    /**
     * Calculates the fees owed by the borrower if the item is overdue.
     *
     * @param currentDate the date to be used in the calculation
     * @return the amount of overdue fees.
     */
    public Money calculateFees(GregorianCalendar currentDate)
    {
        long numberOfMSInADay = 1000 * 60 * 60 * 24;
        long span;
        span = currentDate.getTimeInMillis() - this.dueDate.getTimeInMillis();
        if (span < 0)
        {
            return new Dollar(0.0);
        }
        double result = Math.floor(span / numberOfMSInADay) * this.lateFee;
        return new Dollar(result);
    }
}
