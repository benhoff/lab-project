import static org.junit.Assert.*;

import org.junit.Test;
import java.util.GregorianCalendar;

/**
 * The test class AbstractMediaItemTest.
 *
 * @author Ben Hoff
 * @version 1.0
 */
public abstract class AbstractMediaItemTest 
{
    private static final String CALL_NUMBER = "CALL NUMBER";
    private static final int FIRST_COPY = 1;
    private static final String TITLE = "TITLE 1";
    private static final GregorianCalendar DAY_1 = 
                                new GregorianCalendar(2012, 9, 1);
    private static final GregorianCalendar DAY_2 = 
                                new GregorianCalendar(2012, 9, 2);
    private static final GregorianCalendar DAY_15 =
                                new GregorianCalendar(2012, 9, 15);
    private static final String BORROWER = "BORROWER 1";
    private static final Money NO_FEE = new Dollar(0.0);
    private static final Money ONE_DAY_FEE = new Dollar(0.50);
    private static final String UPPER_CASE = "AAAAAA";
    private static final String LOWER_CASE = "zzzzzz";
    private static final String NUMBERS = "121212";
    private static final String DECIMAL_CALL = "BOOK12.34";
    private static final String SPACE_CALL = "BOOK1 1234";
    
    
    public static final int COPY_ZERO = 0;
    private static final int COPY_MINUS_ONE = -1;
    private static final String SPECIAL_CHARS = "!@#$%,/;";
    
    private AbstractMediaItem ami;
    
    /**
     * Use the constructor to instantiate the object in the abstract test.
     */
    public AbstractMediaItemTest()
    {
        //create the test item on constructor.
        //this has to be done or tests won't
        //execute
        ami = createItem(); 
    }
    
    /**
     * Test media item mutator and accessor methods.
     */
    @Test
    public void testItemGetSetMethods()
    {
        AbstractMediaItem a = createItem();
        a.setCallNumber(CALL_NUMBER);
        assertEquals("Hint: getCallNumber() return value does not match " +
                     "set value. ",
                     CALL_NUMBER, a.getCallNumber());
        a.setCopyNumber(FIRST_COPY);
        assertEquals("Hint: getCopyNumber() return value does not match " +
                     "set value. ",
                     FIRST_COPY, a.getCopyNumber());
        a.setTitle(TITLE);
        assertEquals("Hint: getTitle() return value does not match " +
                     "set value. ",
                     TITLE, a.getTitle());
        a.setDueDate(DAY_1);
        assertEquals("Hint: getDueDate() return value does not match " +
                     "set value. ",
                     DAY_1, a.getDueDate());
        a.setBorrower(BORROWER);
        assertEquals("Hint: getBorrower() return value does not match " +
                     "set value. ",
                      BORROWER, a.getBorrower());
    }

    /**
     * Test null call number
     */
    public void testNullSetCallNumber()
    {
        boolean error = false;
        try {
            AbstractMediaItem a = createItem();
            a.setCallNumber(null);
            assertNull(a.getCallNumber());
        }
        catch (IllegalArgumentException e) {
            error = true;
        }
        assertTrue(error);
    }

    /**
     * Test empty call number
     */
    public void testEmptySetCallNumber()
    {
        boolean error = false;
        try {
            AbstractMediaItem a = createItem();
            a.setCallNumber("");
            assertTrue(a.getCallNumber().isEmpty());
        }
        catch (IllegalArgumentException e) {
            error = true;
        }
        assertTrue(error);
    }
    
    /**
     * Test multiple space call number
     */
    public void testMultipleSpaceSetCallNumber()
    {
        boolean error = false;
        try {
            AbstractMediaItem a = createItem();
            a.setCallNumber("AA AA AA");
            assertEquals("AA AA AA", a.getCallNumber());
        }
        catch (IllegalArgumentException e) {
            error = true;
        }
        System.out.println("mader it here");
        assertTrue(error);
    }
    
    /**
     * Test special  characters is set call number
     */
    public void testSpecialCharactersSetCallNumber()
    {
        boolean error = false;
        
        try {
                
            AbstractMediaItem a = createItem();
            a.setCallNumber(SPECIAL_CHARS);
            assertEquals(SPECIAL_CHARS, a.getCallNumber());
        }
        catch (IllegalArgumentException e) {
            error = true;
        }
        
        assertTrue(error);
    }
    
    /**
     * Test multiple period call number
     */
    public void testMultiplePeriodSetCallNumber()
    {
        boolean error = false;
        try {
            AbstractMediaItem a = createItem();
            a.setCallNumber("AA.AA.AA");
            assertEquals("AA.AA.AA", a.getCallNumber());
        }
        catch (IllegalArgumentException e)
        {
            error = true;
        }
        assertTrue(error);
    }
    
    /**
     * Test calculating the overdue fees.
     */
    @Test
    public void testCalculateFeesMethod()
    {
        AbstractMediaItem a = createItem();
        String feeString = "50";
        
        // Test when the current date is before the due date
        a.setDueDate(DAY_15);
        assertEquals("Hint: calculateFees() should return 0.0 " +
                     "if current date is before due date. ",
                     NO_FEE, a.calculateFees(DAY_1));

        // Test when the current date equals the due date
        assertEquals("Hint: calculateFees() should return 0.0 " +
                     "if current date equals due date. ",
                     NO_FEE, a.calculateFees(DAY_15));

        // Test when the current date is one day after the due date
        a.setDueDate(DAY_1);
        assertEquals("Hint: calculateFees() should return a fee " +
                     "equal to " + feeString +
                     " cents for every day overdue. ",
                     ONE_DAY_FEE, a.calculateFees(DAY_2));

        // Test when the current date is two weeks after the due date
        assertEquals("Hint: calculateFees() should return a fee " +
                     "equal to " + feeString + 
                     " cents for every day overdue. ",
                     ONE_DAY_FEE.mul(14),
                     a.calculateFees(DAY_15));                
    }
    
    /**
     * Retrieves a concrete item object for testing.
     * 
     * @return the item object for testing.
     */
    protected abstract AbstractMediaItem createItem();
    

    /**
     * Test to make sure CallNumber cannot be null.
     */
    public void testCallNumberNullVersion2()
    {
        boolean error = false;
        try {
            assertNotNull("Could not create AbstractMediaItem "
            + "from factory pattern"
            , ami);
            ami.setCallNumber(null);
        }
        catch (IllegalArgumentException e) {
            error = true;
        }
        
        assertTrue(error);
    }
    
    /**
     * Test to make sure can set copy number to zero
     */
    @Test
    public void testSetCopyNumber()
    {        
        ami.setCopyNumber(0);
        assertEquals(0, ami.getCopyNumber());
        assertTrue(true);
    }
    
    /**
     * Test to make sure CallNumber cannot be negative.
     */   
    public void testSetCopyNegativeNumber()
    {
        boolean error = false;
        try {
            assertNotNull("Could not create AbstractMediaItem "
                          + "from factory pattern"
                          , ami);
            ami.setCopyNumber(COPY_MINUS_ONE);
        }
        catch (IllegalArgumentException e) {
            error = true;
        }
        
        assertTrue(error);
    }
    
    /**
     * Test to make sure we're not throwing on valid use cases 
     * for setting Call number
     */
    @Test
    public void testNoThrowSetCallNumber()
    {
        AbstractMediaItem a = this.createItem();
        a.setCallNumber(UPPER_CASE);
        assertEquals(UPPER_CASE, a.getCallNumber());
        a.setCallNumber(LOWER_CASE);
        assertEquals(LOWER_CASE, a.getCallNumber());
        a.setCallNumber(NUMBERS);
        assertEquals(NUMBERS, a.getCallNumber());
        a.setCallNumber(DECIMAL_CALL);
        assertEquals(DECIMAL_CALL, a.getCallNumber());
        a.setCallNumber(SPACE_CALL);
        assertEquals(SPACE_CALL, a.getCallNumber());
    }
    
    /**
     * expect throw on empty borrower string
     */
    public void testBorrowerIdException()
    {
        boolean error = false;
        try {
            AbstractMediaItem a = createItem();
            a.setBorrower("");
            assertTrue(a.getBorrower().isEmpty());
        }
        catch (IllegalArgumentException e)
        {
            error = true;
        }
        assertTrue(error);
        
    }
    
    /**
     * test setting and getting due date
     */
    @Test
    public void testGetSetDueDate()
    {
        GregorianCalendar d = new GregorianCalendar();
        d.set(1, 1, 1);
        this.ami.setDueDate(d);
        assertEquals(d, this.ami.getDueDate());
    }
    
    /**
     * throw on set null title
     */
    public void testTitleNull()
    {
        boolean error = false;
        try {
            AbstractMediaItem a = createItem();
            a.setTitle(null);
            assertNull(a.getTitle());
        }
        catch (IllegalArgumentException e)
        {
            error = true;
        }
        
        assertTrue(error);
    }
    
    /**
     * throw on set empyt title
     */
    public void testTitleEmpty()
    {
        boolean error = false;
        try {
            AbstractMediaItem a = createItem();
            a.setTitle("");
            assertTrue(a.getTitle().isEmpty());
        }
        catch (IllegalArgumentException e)
        {
            error = true;
        }
        
        assertTrue(error);
    }
    
}
