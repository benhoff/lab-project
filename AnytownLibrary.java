import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Represents a library.
 * 
 * @author Ben Hoff
 * @version 1.0
 */
public class AnytownLibrary implements Library
{
    /**
     * The maximum number of items allowed in the library.
     */
    private MatchMaker matchMaker;

    // instance variables
    private List<MediaItem> items;
    
    /**
     * Constructor for objects of class AnytownLibrary.
     */
    public AnytownLibrary()
    {
        this.items = new ArrayList<MediaItem>();
    }
    
    /**
     * Constructor that accepts all the items
     * @param mediaItems media items to add to the library
     */
    public AnytownLibrary(List<MediaItem> mediaItems, MatchMaker match)
    {
        // while (mediaItems.size() > AnytownLibrary.MAX_ITEMS)
        // {
        //    mediaItems.remove(mediaItems.size() - 1);
        // }
        this.items = new ArrayList<MediaItem>(mediaItems);
        this.matchMaker = match;
    }
    
    /**
     * Sets the match maker object that encapsulates the search algorithm.
     *
     * @param matcher the object to be used to find items in the library
     */
    public void setMatchMaker(MatchMaker matcher)
    {
        this.matchMaker = matcher;
    }

    /**
    * Creates and returns a list of media items that match the criterion.
    * If the <code>MatchMaker</code> was not set prior to this method being
    * called, the method will return <code>null</code>.
    * If no items are found, the method will return an empty list.
    *
    * @param criterion the search criterion
    * @return a list of matching media items or <code>null</code>.
    */
    public List<MediaItem> searchItems(String criterion)
    {
        return this.matchMaker.search(criterion, this.items);
    }
    
    public List<MediaItem> searchItems(MatchMaker match)
    {
        List<MediaItem> result = new ArrayList<MediaItem>();
        for (MediaItem item : this.items)
        {
            if (match.matches(item))
            {
                result.add(item);
            }
        }
        
        return result;
    }

    /**
     * Adds an item to the library. The call number within the item does
     * not exist in the library, the copy number within the item will be 
     * set to 1 by this method. If the copies of the item already exist in
     * the library, the copy number will be set to the next copy number. For
     * example, if the highest copy number is 3, the item added will be copy
     * number 4.                              
     * 
     * @param  item  the item to be added to library.
     * @return      <code>true</code> if the item was added successfully, 
     *              and <code>false</code> if the item was not added.
     */
    public boolean addItem(MediaItem item)
    {
        if ((item == null))
        {
           return false;
        }
        
        int highestCopy = 0;
        
        for (int i = 0; i < this.items.size(); i++)
        {
            MediaItem anItem = items.get(i);
            if (item.getCallNumber().equals(anItem.getCallNumber()) &&
                            anItem.getCopyNumber() > highestCopy)
            {
                highestCopy = anItem.getCopyNumber();
            }
        }
        
        item.setCopyNumber(highestCopy + 1);
        items.add(item);
        return true;
    }
    
    /**
     * Finds an item by call number and copy number.
     * 
     * @param callNumber the call number
     * @param copy the copy number
     * @return the index of the item, or -1 if not found
     */
    private int findItem(String callNumber, int copy)
    {
        for (int i = 0; i < this.items.size(); i++)
        {
            MediaItem item = items.get(i);
            if (callNumber.equals(item.getCallNumber()) &&
                            item.getCopyNumber() == copy)
            {
                return i;
            }
        }
        
        return -1;
    }
    
    /**
     * Deletes a item from the library.
     * 
     * @param  callNumber  the call number of the item to be deleted.
     * @param  copy        the copy number of the item to be deleted.     
     * @return      <code>true</code> if the item was deleted successfully, 
     *              and <code> false</code> if the item was not deleted.
     */
    public boolean deleteItem(String callNumber, int copy)
    {
        if (callNumber == null)
        {
            return false;
        }

        int index = findItem(callNumber, copy);

        if (index == -1)
        {
            return false;
        }
        this.items.remove(index);
        return true;
    }
    
    /**
     * Checks out an item from the library. The borrower id and due date are
     * set in the item. The due date is set to 30 days from the current date.
     * 
     * @param  callNumber  the call number of the item to be checked out
     * @param  copy        the copy of the item to be checked out
     * @param  borrower    the id of the person borrowing the item
     * @param  dateCheckedOut  the day that the item is checked out
     * @return      the due date if the item was checked out successfully,
     *              and <code> null </code> if is was not.
     */
    public GregorianCalendar checkOut(String callNumber, 
                                      int copy, String borrower,
                                      GregorianCalendar dateCheckedOut)
    {
        int index = findItem(callNumber, copy);
        
        if (index == -1)
        {
            return null;
        }
        
        MediaItem item = items.get(index);
        
        //  Fail if the item is already checked out
        if (item.getDueDate() != null)
        {
            return null;
        }
        
        //  Set the due date 30 days from now
        GregorianCalendar dueDate = (GregorianCalendar)dateCheckedOut.clone();
        dueDate.add(Calendar.DAY_OF_MONTH, 30);
        item.setDueDate(dueDate); 
        item.setBorrower(borrower);
        return dueDate;
    }

    /**
     * Checks an item back into the library. The borrower id and due date are
     * set to <code> null </code> in the item. 
     * 
     * @param  callNumber  the call number of the item to be checked in
     * @param  copy        the copy of the item to be checked in
     * @return      <code>true</code> if the item was checked in successfully, 
     *              and <code> false</code> if it was not.
     */
    public boolean checkIn(String callNumber, int copy)
    {
        int index = findItem(callNumber, copy);
        
        if (index == -1)
        {
            return false;
        }
        
        MediaItem item = items.get(index);
        
        //  Fail if it wasn't checked out
        if (item.getBorrower() == null || item.getDueDate() == null)
        {
            return false;
        }
        
        item.setBorrower(null);
        item.setDueDate(null);
        return true;
    }

    /**
     * Creates and returns an array of media items that match the call number.
     * The length of the returned array is equal to the number of media items
     * that match the call number, and each element in the array is one of the
     * matching media items.
     * 
     * @param callNumber the call number of the media items
     * @return an array of matching media items.
     */
    public MediaItem [] findItems(String callNumber)
    {
        int matchCount = 0;
        for (int i = 0; i < this.items.size(); i++)
        {
            if (callNumber.equals(items.get(i).getCallNumber()))
            {
                matchCount++;
            }
        }
        MediaItem [] found = new MediaItem[matchCount];
        matchCount = 0;
        for (int i = 0; i < this.items.size(); i++)
        {
            MediaItem item = items.get(i);
            if (callNumber.equals(item.getCallNumber()))
            {
                found[matchCount++] = item;
            }
        }
        return found;
    }
    
    private static long millisPerDay = 24 * 60 * 60 * 1000;
    
    /**
     * Returns the difference between the supplied dates in days.
     * 
     * @param startDate the starting date
     * @param endDate the ending date
     * @return an integer representing the data diff
     */
    public static int calculateDateDiff(GregorianCalendar startDate, 
        GregorianCalendar endDate)
    {
        //  This contains a bug related to daylight savings time, 
        //  but is good enough for a lab.
        return (int)((endDate.getTimeInMillis() - 
                        startDate.getTimeInMillis()) / millisPerDay);
    }
    
    /**
     * Adds 14 days to the due date of the given book, and
     * returns the new due date.
     * 
     * @param callNumber the call number
     * @param copy the copy number
     * @param borrower the borrower id
     * @param currentDate the date the renewal was requested
     * @return the new due date
     */
    public GregorianCalendar renew(String callNumber, 
        int copy, String borrower, GregorianCalendar currentDate)
    {
        for (int i = 0; i < this.items.size(); i++) 
        {
            MediaItem item = this.items.get(i);
            if (item.getCallNumber() == callNumber)
            {
                // Only books can be renewed
                if (!(item instanceof Book))
                {
                    return null;
                }
                // Only original borrowers can renew books
                if (!item.getBorrower().equals(borrower))
                {
                    return null;
                }
                
                // Only copy number checked out to borrowers should be renewed
                if (item.getCopyNumber() != copy)
                {
                    return null;
                }
                GregorianCalendar dueDate = item.getDueDate();
                int diff = AnytownLibrary.calculateDateDiff(dueDate,
                                                            currentDate);
                
                if ( diff > 0)
                {
                    return null;
                }
                GregorianCalendar end = (GregorianCalendar) dueDate.clone();
                end.add(Calendar.DAY_OF_MONTH, 14);

                item.setDueDate(end);
                return end;
            }
        }
        return null;
    }
    
    /**
     * Renews all books checked out to the given borrower.
     * 
     * @param borrower the borrower
     * @param currentDate the date the renewal was requested
     * @return an array containing the books that were renewed
     */
    public Book[] renew(String borrower, GregorianCalendar currentDate)
    {
        List<Book> result = new ArrayList<Book>();
        for (int i = 0; i < this.items.size(); i++)
        {
            MediaItem item = this.items.get(i);
            if (item.getBorrower() == borrower && item instanceof Book)
            {
                GregorianCalendar renewed = this.renew(item.getCallNumber(),
                        item.getCopyNumber(),
                        borrower,
                        currentDate);
                
                if (renewed == null)
                {
                    continue;
                }
                result.add((Book) item);
            }
        }
        
        return result.toArray(new Book[result.size()]);
    }
    


    /**
     * Creates and returns an array containing all items in the library.
     *
     * @return the array
     */
    public MediaItem [] getItems()
    {
        MediaItem [] copy = new MediaItem[this.items.size()];
        copy = this.items.toArray(copy); 

        return copy;
    }

    /**
     * Recreates the library from a persisted object file.  The file should
     * have been created by serialization of a prior library object.
     *
     * @param fileName the name of the file containing the library object
     * @return the AnytownLibrary object read from the file
     */
    public static AnytownLibrary readFromFile(String fileName)
        throws IOException, ClassNotFoundException
    {
        FileInputStream fileStream = new FileInputStream(fileName);
        ObjectInputStream objectStream = new ObjectInputStream(fileStream);
        
        List<MediaItem> objects = (ArrayList<MediaItem>) objectStream.readObject();
        MatchMaker match = (MatchMaker) objectStream.readObject();
        objectStream.close();
        fileStream.close();

        return new AnytownLibrary(objects, match);
    }

    /**
     * Writes the current library to the given file name using
     * object serialization.
     *
     * @param fileName the name of the file to write.
     */
    public void writeToFile(String fileName) throws IOException
    {  
        FileOutputStream fileStream = new FileOutputStream(fileName);
        ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);

        objectStream.writeObject(items);
        objectStream.writeObject(this.matchMaker);
        objectStream.close();
        fileStream.close();
    }
}
