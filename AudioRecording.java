/**
 * Track a audio recording for the Anytown library
 * 
 * @author Ben Hoff 
 * @version 24 Feb 2019
 */
public class AudioRecording extends AbstractMediaItem
{
    private String artist;

    /**
     * Constructor for objects of class AudioRecording.
     */
    public AudioRecording()
    {
        super();
        this.setLateFee(0.50);
    }
    
    /**
     * Checks for valid artist
     * @param inArtist artist name
     * @return validity value
     */
    private boolean validAuthor(String inArtist)
    {
        if (inArtist == null)
        {
            return false;
        }
        return !inArtist.isEmpty();
    }
    
    /**
     * sets Artist
     * @param inArtist artist name
     */
    public void setArtist(String inArtist)
    {
        boolean valid = this.validAuthor(inArtist);
        if (!valid)
        {
            throw new IllegalArgumentException();
        }
        
        this.artist = inArtist;
    }
    
    /**
     * gets artist
     * @return artist name
     */
    public String getArtist()
    {
        return this.artist;
    }
}
