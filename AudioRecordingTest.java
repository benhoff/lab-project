import org.junit.*;
import static org.junit.Assert.*;
/**
 * The test class AudioRecordingTest.
 *
 * @author Ben Hoff
 * @version 2.0
 */
public class AudioRecordingTest extends AbstractMediaItemTest
{   
    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        // Set up test fixture
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
        // Tear down test fixture
    }
    
    /**
     * test get set artist
     */
    @Test
    public void testGetSetArtist()
    {
        AudioRecording a = new AudioRecording();
        assertNull(a.getArtist());
        a.setArtist("Blah");
        assertEquals("Blah", a.getArtist());
    }
    
    /**
     * test to make sure we throw on empty string
     */
    public void testThrowEmptyArtist()
    {
        boolean error = false;
        AudioRecording a = new AudioRecording();
        try {
            a.setArtist("");
        }
        catch (IllegalArgumentException e)
        {
            error = true;
        }
        assertTrue(error);
    }
    
    /**
     * Test to make sure we throw on null artist
     */
    public void testThrowNullArtist()
    {
        boolean error = false;
        AudioRecording a = new AudioRecording();
        try {
            a.setArtist(null);
        }
        catch (IllegalArgumentException e)
        {
            error = true;
        }
        assertTrue(error);
    }

    /**
     * Creates a concrete item to test. DO NOT MODIFY THIS METHOD.
     * 
     * @see AbstractMediaItemTest#createItem()
     * @return the item
     */
    protected AbstractMediaItem createItem()
    {
        return new AudioRecording();
    }
}
