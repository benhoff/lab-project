import java.util.ArrayList;
import java.util.List;
/**
 * Matches the author
 * 
 * @author Ben Hoff 
 * @version 0.0.1
 */
public class AuthorMatch implements MatchMaker
{
    private String criteria;
    
    public AuthorMatch(String inCritera)
    {
        this.criteria = inCritera;
    }
    public List<MediaItem> search(String criterion, List<MediaItem> items)
    {
        List<MediaItem> result = new ArrayList<MediaItem>();
        for (MediaItem item : items)
        {
            if (item instanceof Book)
            {
                Book book = (Book) item;
                String[] split = book.getAuthor().split(" ");
                String lastName = split[split.length - 1];
                if (lastName.equals(criterion))
                {
                    result.add(item);
                }
            }
        }
        return result;
    }
    
    public boolean matches(MediaItem item)
    {
        if (item instanceof Book)
        {
            Book book = (Book) item;
            String[] split = book.getAuthor().split(" ");
            String lastName = split[split.length - 1];
            return lastName.equals(this.criteria);
        }
        
        return false;
    }
}