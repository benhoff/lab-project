import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class AuthorMatchTest {

    @Test
    public void testSearch() {
        AuthorMatch match = new AuthorMatch("this");
        List<MediaItem> items = new ArrayList<MediaItem>();
        Book book1 = new Book();
        book1.setAuthor("this blue");
        Book book2 = new Book();
        book2.setAuthor("red");
        items.add(book1);
        items.add(book2);
        items.add(new VideoRecording());
        
        List<MediaItem> result = match.search("blue", items);
        assertTrue(result.get(0).equals(book1));
        assertEquals(1, result.size());
    }
    
    @Test
    public void testMatch() {
        AuthorMatch match = new AuthorMatch("blue");
        Book book1 = new Book();
        book1.setAuthor("this blue");
        assertTrue(match.matches(book1));
        assertFalse(match.matches(new VideoRecording()));
    }

}
