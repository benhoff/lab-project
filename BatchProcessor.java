import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;
/**
 * Processes library transactions from a file.
 * 
 * @author Ben Hoff
 * @version 0.0.1
 */
public class BatchProcessor
{
    private Library library;
    
    /**
     * Constructor for objects of class BatchProcessor.
     * 
     * @param lib the library to which the transactions are applied
     */
    public BatchProcessor(Library lib)
    {
        this.library = lib;
    }

    /**
     * Reads the transactions from a file and executes them on
     * the library.
     * 
     * @param fileName the name of the file from which to read
     * @return the number of successfully executed transactions
     */
    public int process(String fileName)
    {
        Scanner scanner;
        
        try {
            scanner = new Scanner(new File(fileName));
        }
        catch (FileNotFoundException e)
        {
            return 0;
        }
        
        List<String> list = new ArrayList<String>();
        while (scanner.hasNextLine()) {
            list.add(scanner.nextLine());
        }
        scanner.close();
        return processList(list);
    }
    
    /**
     * Process the list of parameters
     * @param toProcess Strings associated with the processing
     */
    private int processList(List<String> toProcess)
    {
        int count = 0;
        for (String string : toProcess)
        {
            // Split on commas
            List<String> args = new ArrayList<String>();
            args.addAll(Arrays.asList(string.split(",")));
            
            // get the command
            String command = args.get(0);
            
            // we're going to hand off to the individual handlers so no relevent
            args.remove(0);
            // Command checking
            if (command.equalsIgnoreCase("add"))
            {
                count += this.processAdd(args);
            }
            else if (command.equalsIgnoreCase("delete"))
            {
                count += this.processDelete(args);
            }
            else if (command.equalsIgnoreCase("checkout"))
            {
                count += this.processCheckout(args);
            }
            else if (command.equalsIgnoreCase("checkin"))
            {
                count += this.processCheckin(args);
            }
            else if (command.equalsIgnoreCase("renew"))
            {
                count += this.processRenew(args);
            }
            else if (command.equalsIgnoreCase("renewall"))
            {
                count += this.processRenewAll(args);
            }
            // Per spec, invalid commands are ignored so no `else`
             
        }
        return count;
    }
    
    /**
     * get a book item back from the arguments
     * @param args arguments describing the book
     * @return the newly created Book object
     */
    private MediaItem getBook(List<String> args)
    {
        String title = null;
        String author = null;
        for (String arg : args)
        {
            String [] split = arg.split("=");
            if (split[0].equalsIgnoreCase("title"))
            {
                title = split[1];
            }
            else if (split[0].equalsIgnoreCase("author"))
            {
                author = split[1];
            }
        }
        Book book = new Book();
        try {
            book.setAuthor(author);
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }
        
        try {
            book.setTitle(title);
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        return book;
    }
    
    /**
     * create the audio instance associated with the arguments
     * @param args the arguments that make up the audio instance
     * @return an instance of the audio
     */
    private MediaItem getAudio(List<String> args)
    {
        String artist = null;
        String title = null;
        
        for (String arg: args)
        {
            String [] split = arg.split("=");
            if (split[0].equalsIgnoreCase("title"))
            {
                title = split[1];
            }
            else if (split[0].equalsIgnoreCase("artist"))
            {
                artist = split[1];
            }
        }
        AudioRecording audio = new AudioRecording();
        try {
            audio.setArtist(artist);
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }
        
        if (title != null) {
            audio.setTitle(title);
        }
        return audio;
    }
    
    /**
     * create a video with the associated values
     * @param args arguments describining the video
     * @return a newly created video instance
     */
    private MediaItem getVideo(List<String> args)
    {
        String title = null;
        for (String arg: args)
        {
            String [] split = arg.split("=");
            if (split[0].equalsIgnoreCase("title"))
            {
                title = split[1];
            }
        }
        VideoRecording video = new VideoRecording();
        try {
            video.setTitle(title);
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }
        
        return video;
    }
    
    /**
     * process the `add` command
     * @param args the arguments to process
     */
    private int processAdd(List<String> args)
    {
        if (args.size() < 2)
        {
            return 0;
        }
        String type = args.get(0);
        String callNumber = args.get(1);
        args.remove(0);
        args.remove(0);

        
        MediaItem item = null;
        if (type.equalsIgnoreCase("book"))
        {
            item = this.getBook(args);
        }
        else if (type.equalsIgnoreCase("audio"))
        {
            item = this.getAudio(args);
        }
        else if (type.equalsIgnoreCase("video"))
        {
            item = this.getVideo(args);
        }
        else
        {
            return 0;
        }
        
        item.setCallNumber(callNumber);
        boolean added = this.library.addItem(item);
        if (added)
        {
            return 1;
        }
        return 0;
    }
    
    /**
     * process the delete command
     * @param args the arguments associated with the delete command
     */
    private int processDelete(List<String> args)
    {
        String callNumber = args.get(0);
        int copyNumber = Integer.parseInt(args.get(1));
        boolean deleted = this.library.deleteItem(callNumber, copyNumber);
        if (deleted)
        {
            return 1;
        }
        return 0;
    }
    
    /**
     * process the checkout command
     * @param args the arguments associated with the checkout
     */
    private int processCheckout(List<String> args)
    {
        String callNumber = args.get(0);
        int next = 1;
        int copyNumber = -1;
        try {
            copyNumber = Integer.parseInt(args.get(1));
            next += 1;
        }
        catch (NumberFormatException e)
        {
            MediaItem [] items = this.library.findItems(callNumber);
            for (int i = 0; i < items.length; i++)
            {
                MediaItem item = items[i];
                if (item.getBorrower() == null)
                {
                    copyNumber = item.getCopyNumber();
                    break;
                }
            }
            
        }
        String borrowerId = args.get(next);
        next += 1;
        GregorianCalendar date = BatchProcessor.dateHelper(args.get(next));
        // System.out.println(date.getTime());
        
        this.library.checkOut(callNumber, copyNumber, borrowerId, date);
        // System.out.println(checked.getTime());
        // System.out.println(callNumber + " " + copyNumber + " " + borrowerId);
        return 1;
    }
    
    /**
     * process the `checkin` command
     * @param args the arguments associated with a checkin
     */
    private int processCheckin(List<String> args)
    {
        String callNumber = args.get(0);
        int copyNumber = Integer.parseInt(args.get(1));
        this.library.checkIn(callNumber, copyNumber);
        return 1;
    }
    
    /**
     * process the renew command
     * @param args associated renew arguments
     */
    private int processRenew(List<String> args)
    {
        String callNumber = args.get(0);
        int copyNumber = Integer.parseInt(args.get(1));
        String borrowerId = args.get(2);
        GregorianCalendar date = BatchProcessor.dateHelper(args.get(3));
        this.library.renew(callNumber, copyNumber, borrowerId, date);
        return 1;
        
    }
    
    /**
     * process the renewall command
     * @param args the associated arguments
     */
    private int processRenewAll(List<String> args)
    {
        String borrower = args.get(0);
        GregorianCalendar date = BatchProcessor.dateHelper(args.get(1));
        this.library.renew(borrower, date);
        return 1;

    }
    
    /**
     * converts string into GregorianCalendar
     * @param date the string date to change
     * @return a new date
     */
    static public GregorianCalendar dateHelper(String date)
    {
        int month = Integer.parseInt(date.substring(0, 2));
        int day = Integer.parseInt(date.substring(2, 4));
        int year = Integer.parseInt(date.substring(4));
        return new GregorianCalendar(year, month, day, 0, 0);
    }
    
    /**
     * Reads the transactions from the given Reader,
     * executing them on the library.
     * 
     * @param reader the reader from which to read
     * @return the number of successfully executed transactions
     */
    public int process(Reader reader)
    {
        int count = 0;
        BufferedReader buffer = new BufferedReader(reader);
        List<String> list = new ArrayList<String>();
        boolean hasNext = true;
        String line = null;
        while (hasNext)
        {
            try {
                line = buffer.readLine();
            }
            catch (IOException e)
            {
                break;
            }
            if (line == null)
            {
                hasNext = false;
            }
            else 
            {
                list.add(line);
                count += 1;
            }
        }
        processList(list);
        return count;
    }
}
