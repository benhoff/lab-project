import org.junit.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import static org.junit.Assert.*;

/**
 * The test class BatchProcessorTest.
 *
 * @author  Ben Hoff
 * @version 0.0.1
 */
public class BatchProcessorTest 
{
    /**
     * Default constructor for test class BatchProcessorTest.
     */
    public BatchProcessorTest()
    {
        // Default constructor
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        // Set up
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
        // Tear down
    }
    
    /**
     * Test the constructor
     */
    @Test
    public void testConstructor()
    {
        BatchProcessor batch = new BatchProcessor(null);
        assertNotNull(batch);
    }
    
    /**
     * Test the date helper
     */
    @Test
    public void testCreateDate()
    {
        
        GregorianCalendar date = BatchProcessor.dateHelper("01022000");
        assertEquals(1, date.get(Calendar.MONTH));
        assertEquals(2, date.get(Calendar.DAY_OF_MONTH));
        assertEquals(2000, date.get(Calendar.YEAR));
        GregorianCalendar newDate = BatchProcessor.dateHelper("03152008");
        assertEquals(3, newDate.get(Calendar.MONTH));
        assertEquals(15, newDate.get(Calendar.DAY_OF_MONTH));
        assertEquals(2008, newDate.get(Calendar.YEAR));
        
        
    }
    
    /**
     * test a count of zero when no file
     */
    @Test
    public void testNoFile()
    {
        BatchProcessor batch = new BatchProcessor(null);
        int result = batch.process("thtawerasdfasdfsafd");
        assertEquals(0, result);
    }
    
    /**
     * Test writer
     */
    @Test
    public void testProcessReader()
    {
        StringWriter write = new StringWriter();
        String s = "ADD,Book,SF CAR,title=Enders Game,author=Orson Scott Card";
        write.write(s);
        StringReader read = new StringReader(write.toString());
        AnytownLibrary lib = new AnytownLibrary();
        BatchProcessor batch = new BatchProcessor(lib);
        batch.process(read);
        MediaItem [] items = lib.getItems();
        assertEquals(1, items.length);
    }

    /**
     * Test process
     */
    @Test
    public void testProcess()
    {
        String file = "/tmp/data.txt";
        try {
            PrintWriter p = new PrintWriter(file);
            String s;
            s = "ADD,Book,SF CAR,title=Enders Game,author=Orson Scott Card";
            p.println(s);
            s = "ADD,audio,CD R137B,artist=Bonnie Raitt,";
            s += "title=The Bonnie Raitt Collection";
            p.println(s);
            p.println("Add,video,DVD MIR,title=Miracle");
            p.println("add,book,123.45,title=placeholder");
            p.println("DELETE,CD R137B,1");
            p.println("CheckOut,SF CAR,1,Jane Doe,03012008");
            p.println("checkout,DVD MIR,Tom Smith,03022008");
            p.println("checkout,123.45,1,Jane Doe,03012008");
            p.println("checkin,DVD MIR,1");
            p.println("renew,SF CAR,1,Jane Doe,03152008");
            p.println("renewAll,Jane Doe,03202008");
            p.println("JUNK,JUNK,JUNK");
            p.println("Add");
            p.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        AnytownLibrary lib = new AnytownLibrary();
        BatchProcessor batch = new BatchProcessor(lib);
        int trans = batch.process(file);
        MediaItem [] items = lib.getItems();
        assertEquals(3, items.length);
        assertEquals(11, trans);
        System.out.println(items[0].getDueDate().getTime());
        MediaItem [] checked = lib.findItems("123.45");
        System.out.println(checked[0].getDueDate().getTime());

        // Hint: Wrong number of items in library after processing a three 
        // ADD commands that contained only the item type and call number.

        // Hint: Wrong number of items in library after processing a three 
        // ADD commands that contained the item type, call number, and a bad 
        // field name.

        // Hint: Due date was not set correctly when command checkout,123.45,1
        // ,Jane Doe,03012008 was processed.


        
    }
}
