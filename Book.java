/**
 * Tracks books checked in/out of the library
 * 
 * @author Ben Hoff
 * @version 1.0
 */

public class Book extends AbstractMediaItem
{
    // instance variables

    private String author;

    
    /**
     * Returns the name of the author.
     * 
     * @return  the name of the author.
     */
    public String getAuthor()
    {
        return this.author;
    }    
    
    /**
     * Sets the name of the author.
     * 
     * @param   name    the name of the author.
     */
    public void setAuthor(String name)
    {
        if (name == null)
        {
            throw new IllegalArgumentException();
        }
        else if (name.isEmpty())
        {
            throw new IllegalArgumentException();
        }
        
        this.author = name;
    }
}
