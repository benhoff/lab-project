import org.junit.*;
import static org.junit.Assert.*;
import java.util.GregorianCalendar;

/**
 * The test class BookTest.
 *
 * @author Ben Hoff
 * @version 2.0
 */
public class BookTest extends AbstractMediaItemTest
{   

    private static final GregorianCalendar DAY_1 = 
                                new GregorianCalendar(2012, 9, 1);
    private static final GregorianCalendar DAY_2 = 
                                new GregorianCalendar(2012, 9, 2);
    private static final GregorianCalendar DAY_15 =
                                new GregorianCalendar(2012, 9, 15);
    private static final Money NO_FEE = new Dollar(0.0);
    private static final Money ONE_DAY_FEE = new Dollar(0.25);

    /**
     * Creates a concrete item to test. DO NOT MODIFY THIS METHOD.
     * 
     * @see AbstractMediaItemTest#createItem()
     * @return the item
     */
    protected AbstractMediaItem createItem()
    {
        return new Book();
    }
    
    /**
     * Test the constructor
     */
    @Test
    public void testConstructor()
    {
        Book book = (Book) createItem();
        assertNotNull(book);
    }
    
    /**
     * Test get set author
     */
    @Test
    public void testGetSetAuthor()
    {
        Book b = (Book) this.createItem();
        assertNull(b.getAuthor());
        b.setAuthor("A");
        assertEquals("A", b.getAuthor());
    }
    
    /**
     * Test throw on set null author
     */
    public void testNullAuthor()
    {
        boolean error = false;
        
        Book b = new Book();
        try {
            b.setAuthor(null);
        }
        catch (IllegalArgumentException e)
        {
            error = true;
        }
        
        assertTrue(error);
    }
    
    /**
     * Test throw on empty string
     */
    public void testEmptyAuthor()
    {
        boolean error = false;
        
        Book b = new Book();
        try {
            b.setAuthor("");
        }
        catch (IllegalArgumentException e)
        {
            error = true;
        }
        assertTrue(error);
    }
    /**
     * Test calculating the overdue fees.
     */
    @Test
    public void testCalculateFeesMethod()
    {
        AbstractMediaItem a = createItem();
        String feeString = "25";
        
        // Test when the current date is before the due date
        a.setDueDate(DAY_15);
        assertEquals("Hint: calculateFees() should return 0.0 " +
                     "if current date is before due date. ",
                     NO_FEE, a.calculateFees(DAY_1));

        // Test when the current date equals the due date
        assertEquals("Hint: calculateFees() should return 0.0 " +
                     "if current date equals due date. ",
                     NO_FEE, a.calculateFees(DAY_15));

        // Test when the current date is one day after the due date
        a.setDueDate(DAY_1);
        assertEquals("Hint: calculateFees() should return a fee " +
                     "equal to " + feeString +
                     " cents for every day overdue. ",
                     ONE_DAY_FEE, a.calculateFees(DAY_2));

        // Test when the current date is two weeks after the due date
        assertEquals("Hint: calculateFees() should return a fee " +
                     "equal to " + feeString + 
                     " cents for every day overdue. ",
                     ONE_DAY_FEE.mul(14),
                     a.calculateFees(DAY_15));
    }
    
    

}
