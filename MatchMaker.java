import java.io.Serializable;
import java.util.List;
/**
 * Defines an interface for an object that encapsulates a search algorithm.
 *
 * @author Franklin University
 * @version Fall 2016
 */

public interface MatchMaker extends Serializable
{
	/**
	 * Finds all the items in the list that matches the criterion.
	 *
	 * @param  criterion the string to be matched
	 * @param  items the list to be searched for a match
	 * @return	the list of matching items
	 */
	List<MediaItem> search(String criterion, List<MediaItem> items);

	public boolean matches(MediaItem item);
}
