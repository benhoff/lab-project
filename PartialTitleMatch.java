import java.util.ArrayList;
import java.util.List;

/**
 * Matches titles that are partial matches to the criteria
 * 
 * @author Ben Hoff
 * @version 0.0.1
 */
public class PartialTitleMatch implements MatchMaker
{
    private String criteria;
    
    public PartialTitleMatch(String inCriteria)
    {
        this.criteria = inCriteria.toLowerCase();
    }
    
    public List<MediaItem> search(String criterion, List<MediaItem> items)
    {
        criterion = criterion.toLowerCase();
        List<MediaItem> result = new ArrayList<MediaItem>();
        for (MediaItem item : items)
        {
            String title = item.getTitle().toLowerCase();
            if (title.contains(criterion))
                result.add(item);
        }
        return result;
    }
    
    public boolean matches(MediaItem item)
    {
        return item.getTitle().toLowerCase().contains(this.criteria);
    }
}
