import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class PartialTitleMatchTest {

    @Test
    public void testSearch()
    {
        PartialTitleMatch match = new PartialTitleMatch("this");
        List<MediaItem> items = new ArrayList<MediaItem>();
        Book book1 = new Book();
        book1.setTitle("thisblue");
        Book book2 = new Book();
        book2.setTitle("red");
        items.add(book1);
        items.add(book2);
        
        List<MediaItem> result = match.search("this", items);
        assertTrue(result.get(0).equals(book1));
        assertEquals(1, result.size());
    }
    
    @Test
    public void testMatch()
    {
        PartialTitleMatch match = new PartialTitleMatch("this");
        Book book1 = new Book();
        book1.setTitle("thisblue");
        assertTrue(match.matches(book1));

        
    }
}
