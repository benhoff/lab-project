import java.util.ArrayList;
import java.util.List;
/**
 * Matches when the title is the same as the criteria
 * 
 * @author Ben Hoff
 * @version 0.0.1
 */
public class TitleMatch implements MatchMaker
{
    private String critera;
    public TitleMatch(String inCriteria)
    {
        this.critera = inCriteria;
    }
    
    public List<MediaItem> search(String criterion, List<MediaItem> items)
    {
        List<MediaItem> result = new ArrayList<MediaItem>();
        for (MediaItem item : items)
        {
            if (item.getTitle().equalsIgnoreCase(criterion))
                result.add(item);
            
        }
        return result;
    }
    /**
     * Matches the item to the criteria
     * @param item the item to match against
     * @return if the item matches or not
     */
    public boolean matches(MediaItem item)
    {
        return item.getTitle() == this.critera;
    }
}
