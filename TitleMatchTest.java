import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TitleMatchTest {

    @Test
    public void testSearch() {
        TitleMatch match = new TitleMatch("this");
        List<MediaItem> items = new ArrayList<MediaItem>();
        Book book1 = new Book();
        book1.setTitle("thisblue");
        Book book2 = new Book();
        book2.setTitle("red");
        items.add(book1);
        items.add(book2);
        
        List<MediaItem> result = match.search("thisblue", items);
        assertTrue(result.get(0).equals(book1));
        assertEquals(1, result.size());
    }
    
    @Test
    public void testMatch()
    {
        TitleMatch match = new TitleMatch("thisblue");
        Book book1 = new Book();
        book1.setTitle("thisblue");
        assertTrue(match.matches(book1));
    }

}
