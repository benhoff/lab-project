/**
 * Tracks Video recordings checked in/out of the library.
 * 
 * @author Ben Hoff
 * @version 1.0
 */
public class VideoRecording extends AbstractMediaItem
{
    // instance variables
    
    /**
     * Constructor for objects of class VideoRecording.
     */
    public VideoRecording()
    {
        super();
        this.setLateFee(0.5);
    }
}
