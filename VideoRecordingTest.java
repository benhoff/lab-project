import org.junit.*;
import static org.junit.Assert.*;

/**
 * The test class VideoRecordingTest.
 *
 * @author Ben Hoff
 * @version 2.0
 */
public class VideoRecordingTest extends AbstractMediaItemTest
{   
    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        // Set up test fixture
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
        // Tear down test fixture
    }

    /**
     * Creates a concrete item to test. DO NOT MODIFY THIS METHOD.
     * 
     * @see AbstractMediaItemTest#createItem()
     * @return the item
     */
    protected AbstractMediaItem createItem()
    {
        return new VideoRecording();
    }
    
    /**
     * Test constructor
     */
    @Test
    public void testMyConstructor()
    {
        VideoRecording video = new VideoRecording();
        assertNotNull(video);
    }
    
    /**
     * Test second consturocrt
     */
    @Test
    public void testMyConstructor2()
    {
        VideoRecording video = new VideoRecording();
        assertNotNull(video);
    }
}
